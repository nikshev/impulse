//+------------------------------------------------------------------+
//|                                                      Impulse.mq4 |
//|                                Copyright 2014, Eugene $hkurnikov |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "Copyright 2014, Eugene $hkurnikov"
#property link      ""
#property version   "1.00"
#property strict
extern string   resistance="l1";
extern string   support="l2";
extern int        slippage=10;
extern int        magic=1000;
extern double  Lot =0.05;
extern int        BreakEven=100;
//-------Trailing stop------------------------------------------
extern bool   ProfitTrailing = True;  //Only profitable trailer stop
extern int    TrailingStop   = 50;     // Trailing stop points
extern int    TrailingStep   = 50;     // Trailing stop step points
extern bool   UseSound       = True;  // Use sound when trailing stop
extern string NameFileSound  = "expert.wav";  // File for play
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
  string support_line_name=support;
  string resistance_line_name=resistance;
  
  double  SL_sell=0;
  double  TP_sell=0;
  double  SL_buy=0;
  double  TP_buy=0;
  int ticket=-1;
  
  MoveBreakEven();
  /*for (int i=0; i<OrdersTotal(); i++) {
    if (OrderSelect(i, SELECT_BY_POS, MODE_TRADES)) {
      TrailingPositions();
    }
  }*/
  
  WaitForTradeContext();
  if (ObjectFind(support_line_name)==0){
   if (GetSupportTrendLineSignal(support_line_name)){
     DeleteLines(support_line_name);
     DeleteLines(resistance_line_name);
     //Open sell order
     Print("SELL");
     WaitForTradeContext();
     ticket=OrderSend(Symbol(),OP_SELL,Lot,Bid,slippage,SL_sell,TP_sell,"",magic,0,Blue);
     if (ticket<0) {
      Alert("Send order error #",GetLastError(), " SL=",SL_sell," TP=",TP_sell," Bid=",Bid," Lot=",Lot);
      WaitForTradeContext();
      ticket=OrderSend(Symbol(),OP_SELL,Lot,Bid,slippage,0,0,"",magic,0,Blue);
      if (ticket>0)
       OrderModify(ticket,OrderOpenPrice(),SL_sell,TP_sell,0,Blue);
      else {
        Alert("Send order error #",GetLastError(), " SL=",SL_sell," TP=",TP_sell," Bid=",Bid," Lot=",Lot);
       }
     } 
   }
  }
  

  WaitForTradeContext();
  if (ObjectFind(resistance_line_name)==0){
   if (GetResistanceTrendLineSignal(resistance_line_name)){
     DeleteLines(support_line_name);
     DeleteLines(resistance_line_name);
     //Open buy order
     Print("BUY");
     WaitForTradeContext(); 
     ticket=OrderSend(Symbol(),OP_BUY,Lot,Ask,slippage,SL_buy,TP_buy,"",magic,0,Blue);
     if (ticket<0) {
       Alert("Send order error #",GetLastError(), " SL=",SL_buy," TP=",TP_buy," Ask=",Ask," Lot=",Lot);
       WaitForTradeContext();
       ticket=OrderSend(Symbol(),OP_BUY,Lot,Ask,slippage,0,0,"",magic,0,Blue);
       if (ticket>0)
        OrderModify(ticket,OrderOpenPrice(),SL_buy,TP_buy,0,Blue);
       else
        {
         Alert("Send order error  #",GetLastError(), " SL=",SL_buy," TP=",TP_buy," Ask=",Ask," Lot=",Lot);
        }
     }
    }
  }
   
   if (Hour()==18){
     DeleteLines(support_line_name);
     DeleteLines(resistance_line_name);
   }
  
   }
//+------------------------------------------------------------------+


//+------------------------------------------------------------------+
//| Wait For Trade Context                                           |
//+------------------------------------------------------------------+
void WaitForTradeContext()
{
 while(!IsTradeAllowed()) 
  Sleep(300);
 RefreshRates();
}

//+------------------------------------------------------------------+
//| Support line check break                                         |
//+------------------------------------------------------------------+

bool GetSupportTrendLineSignal(string support_line_name)
{  
   string strName=support_line_name;
   double x1 = ObjectGet( strName, OBJPROP_TIME1);
   double y1 = ObjectGet( strName, OBJPROP_PRICE1);
   double x2 = ObjectGet( strName, OBJPROP_TIME2);
   double y2 = ObjectGet( strName, OBJPROP_PRICE2);

   // calculate the slope of the line (m)
   double m = (y2-y1)/(x2-x1);


   // calcualte the offset (b)
   double b = y1 -m*x1;
   // get the current x value
   double time = TimeCurrent();
   // calculate the value (y) of the projected trendline at (x): y = mx + b
   double value = m*time + b;
   if( Ask < value )
    return(true);
   else
    return(false);
}



//+------------------------------------------------------------------+

//| Resistance line check break                           |

//+------------------------------------------------------------------+

bool GetResistanceTrendLineSignal(string resistance_line_name)
{  
   string strName=resistance_line_name;
   // get the four coordinates
   double x1 = ObjectGet( strName, OBJPROP_TIME1);
   double y1 = ObjectGet( strName, OBJPROP_PRICE1);
   double x2 = ObjectGet( strName, OBJPROP_TIME2);
   double y2 = ObjectGet( strName, OBJPROP_PRICE2);

   // calculate the slope of the line (m)
   double m = (y2-y1)/(x2-x1);
   // calcualte the offset (b)
   double b = y1 -m*x1;
   // get the current x value
   double time = TimeCurrent();
   // calculate the value (y) of the projected trendline at (x): y = mx + b
   double value = m*time + b;
   if( Bid > value )
    return(true);
   else
    return(false);
}

//+------------------------------------------------------------------+
//| Delete all lines                                             |
//+------------------------------------------------------------------+
void DeleteLines(string strName){
 if (ObjectFind(strName)==0)
     ObjectDelete(strName);
}

//+------------------------------------------------------------------+
//Move at BE if profit>BE
//+------------------------------------------------------------------+
void MoveBreakEven()
{
  for (int i = 0; i < OrdersTotal(); i++)
   {
    OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
    WaitForTradeContext();
    if(OrderSymbol()==Symbol() && OrderType()==OP_BUY)
     {
      if (OrderMagicNumber()==magic)
       if(High[0]-OrderOpenPrice()>=BreakEven*Point && OrderStopLoss()!=OrderOpenPrice() && Bid>OrderOpenPrice())
        {
           if (!OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,Green))
            Print("Last Error Order Modify="+GetLastError());
          //Print ("MoveBreakEven()"," High[0]=",High[0]);
        }
     }
     else     
     {
      if(OrderSymbol()==Symbol() && OrderType()==OP_SELL)
       {
        if (OrderMagicNumber()==magic)
         if(NormalizeDouble(OrderOpenPrice()-Low[0],5)>=NormalizeDouble(BreakEven*Point,5)  && OrderStopLoss()!=OrderOpenPrice() && NormalizeDouble(Ask,5)<OrderOpenPrice())
          {
            if (!OrderModify(OrderTicket(),OrderOpenPrice(),OrderOpenPrice(),OrderTakeProfit(),0,Green))
             Print("Last Error Order Modify="+GetLastError());
            //Print ("MoveBreakEven()"," Low[0]=",Low[0]);
           }
       }
     }
   }
}

//+------------------------------------------------------------------+
//| Trailing stop function                                     |
//+------------------------------------------------------------------+
void TrailingPositions() {
  double pBid, pAsk, pp;
  
  pp = MarketInfo(OrderSymbol(), MODE_POINT);
  if (OrderType()==OP_BUY) {
    pBid = MarketInfo(OrderSymbol(), MODE_BID);
    if (!ProfitTrailing || (Bid-OrderOpenPrice())>TrailingStop*Point) {
      if (OrderStopLoss()<Bid-(TrailingStop+TrailingStep-1)*Point) {
        ModifyStopLoss(Bid-TrailingStop*Point);
        return;
      }
    }
  }
  if (OrderType()==OP_SELL) {
    pAsk = MarketInfo(OrderSymbol(), MODE_ASK);
    if (!ProfitTrailing || OrderOpenPrice()-Ask>TrailingStop*Point) {
      if (OrderStopLoss()>Ask+(TrailingStop+TrailingStep-1)*Point || OrderStopLoss()==0) {
        ModifyStopLoss(Ask+TrailingStop*Point);
        return;
      }
    }
  }
}
 
//+------------------------------------------------------------------+
//| Modify stop loss function                              |
//+------------------------------------------------------------------+
void ModifyStopLoss(double ldStopLoss) {
  bool fm;
  fm=OrderModify(OrderTicket(),OrderOpenPrice(),ldStopLoss,OrderTakeProfit(),0,CLR_NONE);
  //if (!fm)
   //Print("Last Error="+GetLastError()+" ldStopLoss="+ldStopLoss+" Ask="+Ask);
  if (fm && UseSound) PlaySound(NameFileSound);
}
//+------------------------------------------------------------------+

